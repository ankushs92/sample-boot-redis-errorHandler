package com.example.config;

import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.Cache;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.CacheErrorHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;

import com.example.model.Developer;
@EnableCaching
@Configuration
public class CacheConfig extends CachingConfigurerSupport {
	
	private static final Logger logger = LoggerFactory.getLogger(CacheConfig.class);

	
	@Bean
	public RedisTemplate<String,String> configureRedisTemplate(){
		final RedisTemplate<String,String> redisTemplate = new RedisTemplate<String,String>();
		redisTemplate.setConnectionFactory(configureRedisConnectionFactory());
		//JSON representation of Developer class will be stored in Redis. 
		redisTemplate.setValueSerializer(new Jackson2JsonRedisSerializer<Developer>(Developer.class));
		return redisTemplate;
	}
	
	@Bean
	public RedisConnectionFactory configureRedisConnectionFactory(){
		//Set of Defaults
		final JedisConnectionFactory jedisConnectionFactory = new JedisConnectionFactory();
		return jedisConnectionFactory;
	}
	
	@Bean
	public RedisCacheManager configureCacheManager(){
		final RedisCacheManager redisCacheManager = new RedisCacheManager(configureRedisTemplate());
		return redisCacheManager;
	}
	
	@Override
	public CacheErrorHandler errorHandler(){
			return new CacheErrorHandler(){

				@Override
				public void handleCacheClearError(RuntimeException ex, Cache cache) {
					
					
				}

				@Override
				public void handleCacheEvictError(RuntimeException ex, Cache cache, Object key) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void handleCacheGetError(RuntimeException ex, Cache cache, Object key) {
					logger.warn("Exception occured when fetching an item from Redis.Time : {} , Key : {} , Cache : {}, Exception : ",
								LocalDateTime.now(),
								key,
								cache.getName(),
								ex);
					
				}

				@Override
				public void handleCachePutError(RuntimeException arg0, Cache arg1, Object arg2, Object arg3) {
					// TODO Auto-generated method stub
					
				}
				
			};
	}
	
}

