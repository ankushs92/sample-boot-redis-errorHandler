package com.example

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cache.annotation.EnableCaching


@SpringBootApplication
class SpringRedisTestApplication {

	static void main(String[] args) {
		SpringApplication.run SpringRedisTestApplication, args
	}
}
