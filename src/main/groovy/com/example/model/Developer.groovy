package com.example.model

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name="dev")
class Developer {
	
	@Id
	@GeneratedValue
	Integer id
	
	@Column
	String name
	

}
