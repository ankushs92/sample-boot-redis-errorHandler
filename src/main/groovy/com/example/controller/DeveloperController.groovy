package com.example.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

import com.example.service.DeveloperService;;

@RestController
class DeveloperController {
	
	@Autowired
	private DeveloperService developerService
	
	@GetMapping("developers/{developerId}")
	def listAll(@PathVariable Integer developerId){
		[ data : developerService.findById(developerId) ]
	}
}
