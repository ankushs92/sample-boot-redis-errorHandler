package com.example.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cache.annotation.Cacheable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

import com.example.model.Developer
import com.example.repository.DeveloperRepository;;

@Service
@Transactional
class DeveloperService {
	
	@Autowired
	private DeveloperRepository developerRepository
	
	@Cacheable("devs")
	Developer findById(Integer id){
		developerRepository.findOne(id)
	}
}
