package com.example

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

import com.example.model.Developer
import com.example.repository.DeveloperRepository;;

@RunWith(SpringRunner)
@SpringBootTest
class SpringRedisTestApplicationTests {

	@Autowired
	DeveloperRepository developerRepository
	
	@Test
	void contextLoads() {
		20.times{ i->
			developerRepository.save(new Developer(name:'SomeName' + i))
		}
	}
}
